#pragma once
#include <list>
#include "Observer.h"
class Observable
{
public:
	Observable();
	virtual ~Observable();
	void addObserver(Observer *obj);
	void removeObserver(Observer *obj);
protected:
	void notifyUpdate();
	void disableNotify();
	void enableNotify();
private:
	std::list<Observer*> _observers;
	bool _notify;
};

