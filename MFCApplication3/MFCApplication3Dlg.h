
// MFCApplication3Dlg.h : ���� ���������
//

#pragma once


// ���������� ���� CMFCApplication3Dlg
class CMFCApplication3Dlg : public CDialogEx, public Observer
{
// ��������
public:
	CMFCApplication3Dlg(CWnd* pParent = NULL);	// ����������� �����������

	virtual void update() { return void(); };

// ������ ����������� ����
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCAPPLICATION3_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedMyButton();
	CString TestEdit;
};
