#pragma once
#include "Observable.h"
#include "String.h"
#include <string>

using namespace std;

class Progress : public Observable
{
	
private:
	int _min;
	int _max;
	int _currentProgress;
	string _loadingText;

public:
	void putmin(int min);
	int getmin() { return _min; }
	void putmax(int max);
	int getmax() { return _max; }
	void putprogress(int currentProgress);
	int getcurrentProgress() { return _currentProgress; }
	void putloadingText(string loadingText);
	string getloadingText() { return _loadingText; }

	Progress();
	Progress(string loadingText, int currentProgress);
	Progress(int min, int max);

	__declspec(property(get = getmin, put = putmin)) int min;
	__declspec(property(get = getmax, put = putmax)) int max;
	__declspec(property(get = getcurrentProgress, put = putprogress)) int currentProgress;
	__declspec(property(get = getloadingText, put = putloadingText)) string loadingText;

	string ToString();
};