#include "stdafx.h"
#include "progress.h"
#include <sstream>

using namespace std;

Progress::Progress() : _min(0), _max(100), _currentProgress(0), _loadingText()
{

}

Progress::Progress(string loadingText, int currentProgress) : _min(0), _max(100), _currentProgress(0), _loadingText(loadingText)
{
	assert(_currentProgress >= _min);
	assert(_currentProgress <= _max);
}

Progress::Progress(int min, int max) : _min(0), _max(100), _currentProgress(0), _loadingText()
{
	assert(min <= _max);
	assert(_currentProgress <= _max);
}

void Progress::putprogress(int currentProgress)
{
	assert(currentProgress >= _min);
	assert(currentProgress <= _max);
	if (_currentProgress != currentProgress) 
	{
		_currentProgress = currentProgress;
		notifyUpdate();
	}
}

void Progress::putmin(int min)
{
	assert(min <= _max);
	if (_min != min)
	{
		_min = min;
		if (_currentProgress < _min)
			_currentProgress = _min;
		notifyUpdate();
	}
}

void Progress::putmax(int max)
{
	assert(_min <= max);
	if (_max != max)
	{
		_max = max;
		if (_currentProgress > _max)
			_currentProgress = _max;
		notifyUpdate();
	}
}

void Progress::putloadingText(string loadingText)
{
	if (_loadingText != loadingText)
	{
		_loadingText = loadingText;
		notifyUpdate();
	}
}

string Progress::ToString()
{
	stringstream ss;
	
	ss
		<< "Min = " << _min << ";"
		<< "Max = " << _max << ";"
		<< "Progress = " << _currentProgress << ";";
	
	return ss.str();
}