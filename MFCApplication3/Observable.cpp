#include "stdafx.h"
#include "Observable.h"


Observable::Observable() :_notify(true)
{
}

Observable::~Observable()
{
	_observers.clear();
}

void Observable::addObserver(Observer* obj)
{
	_observers.push_back(obj);
}

void Observable::removeObserver(Observer* obj)
{
	_observers.remove(obj);
}

void Observable::notifyUpdate()
{
	if (!_notify) return;
	std::list<Observer*>::iterator it;
	for (it = _observers.begin(); it != _observers.end(); ++it)
	{
		(*it)->update();
	}
}

void Observable::disableNotify()
{
	_notify = false;
}

void Observable::enableNotify()
{
	_notify = true;
}